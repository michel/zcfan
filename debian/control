Source: zcfan
Section: utils
Priority: optional
Maintainer: Michel Lind <michel@michel-slm.name>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 pandoc,
Standards-Version: 4.7.0
Homepage: https://github.com/cdown/zcfan
Vcs-Browser: https://salsa.debian.org/michel/zcfan
Vcs-Git: https://salsa.debian.org/michel/zcfan.git

Package: zcfan
Architecture: linux-amd64 linux-i386
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts: thinkfan
Description: Zero-configuration fan daemon for ThinkPads
 zcfan is a zero-configuration fan control daemon for ThinkPads.
 .
 Features:
  - Extremely small (~250 lines), simple, and easy to understand code
  - Sensible out of the box, configuration is optional (see "usage" below)
  - Strong focus on stopping the fan as soon as safe to do so,
    without inducing throttling
  - Automatic temperature- and time-based hysteresis:
    no bouncing between fan levels
  - Watchdog support
  - Minimal resource usage
  - No dependencies
